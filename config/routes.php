<?php
use Slim\Http\Request;
use Slim\Http\Response;
use Psr\Log\LoggerInterface ;
use Slim\Container;
use App\Models;

$app->get('/', function (Request $request, Response $response) {    
    return $this->get('view')->render($response, 'home.twig', []);
})->setName('root');

$app->get('/productos', function(Request $request, Response $response){
    $productos = new App\Models\Producto($this);
    return $this->get('view')->render($response, 'products.twig', ['productos'=>$productos->getAll()]);
});

$app->get('/productos/{id}', function(Request $request, Response $response){
    $id = $request->getAttribute('id');
    $productos = new App\Models\Producto($this);
    $producto = $productos->get($id);
    return $this->get('view')->render($response, 'product.twig', ['producto'=>$producto, 'producto_id'=>$id]);
});

$app->post('/productos/{id}', function(Request $request, Response $response){
    $id = $request->getAttribute('id');
    $productos = new App\Models\Producto($this);
    $producto = $productos->get($id);
    $verficacion = ($producto->cantidad >= $_POST['cantidad']);
    $sub_total1 = $_POST['cantidad'] * $producto->precio;
    $sub_total2 = $sub_total1 + 15;
    $impuesto = $sub_total2 * 0.12;
    $total = $sub_total2 + $impuesto;
    $respuesta = ['cantidad' => $_POST['cantidad'],
                    'sub_total1'=>$sub_total1,
                    'envio'=>15,
                    'sub_total2'=>$sub_total2, 
                    'impuesto'=>$impuesto, 
                    'total'=>$total, 
                    'producto' => $producto,
                    'producto_id'=>$id];
    return $this->get('view')->render($response, 'compra.twig', $respuesta);
});

$app->post('/comprar', function(){
    /* CODIGO PARA CONECTARSE A LA URL CON VERBO PUT */
});

$app->put('/api/comprar', function (Request $request, Response $response){
    $post_vars = null;
    parse_str(file_get_contents("php://input"),$post_vars);
    $productos = new App\Models\Producto($this);
    $venta = $productos->sell($post_vars);
    if($venta){
        return $this->get('view')->render($response, 'gracias.twig', []);
    } else {
        return $this->get('view')->render($response, 'oops.twig', []);
    }
});

$app->get('/hello/{name}', function (Request $request, Response $response) {
    $name = $request->getAttribute('name');
    $response->getBody()->write("Hello, $name");

    return $response;
});

$app->get('/time', function (Request $request, Response $response) {
    $viewData = [
        'now' => date('Y-m-d H:i:s')
    ];

    return $this->get('view')->render($response, 'time.twig', $viewData);
});

$app->get('/logger-test', function (Request $request, Response $response) {
    /** @var Container $this */
    /** @var LoggerInterface $logger */

    $logger = $this->get('logger');
    $logger->error('My error message!');

    $response->getBody()->write("Success");

    return $response;
});

// Generando una consulta
use Illuminate\Database\Connection;

$app->get('/databases', function (Request $request, Response $response) {
    /** @var Container $this */
    /** @var Connection $db */
    
    $db = $this->get('db');

    // fetch all rows as collection
    $rows = $db->table('information_schema.schemata')->get();

    // return a json response
    return $response->withJson($rows);
});

$app->post('/admin', function(Request $request, Response $response){
    
        $data = $request->getParsedBody();
        $usuario = new \App\Models\Usuario($data['email'], null, $data['password'], $this);
        $result = $usuario->auth();
        if($result != null){
            $_SESSION['username'] = $result;
            return $this->get('view')->render($response, 'system.twig', ['status'=>'ok', 'info'=>$result[0]->name]);
        } else {
            return $this->get('view')->render($response, 'login.twig', ['status', 'error']);
        }
    
});

$app->get('/admin', function(Request $request, Response $response){
    session_start();
    if(isset($_SESSION['username'])){
        return $this->get('view')->render($response, 'system.twig', ['status'=>'ok', 'info'=>$_SESSION['username']]);
    } else {
        return $this->get('view')->render($response, 'login.twig', ['status', 'error']);
    }
});

$app->get('/logout', function(Request $request, Response $response){
    session_start();
    session_destroy();
    return $this->get('view')->render($response, 'logout.twig', array());
});